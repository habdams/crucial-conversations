<!-- Name this issue "Week 1 - Unbundle with CPR" -->
Spend this first week reflecting on your conversations, especially those that are crucial.

Identify if they are **Content**, **Pattern**, or **Relationship** based.

- [ ] Identify 5 different conversations or situations labeling them with the correct CPR.
- [ ] With R and P, separate the Content of your examples from overarching problem
- [ ] At the end of the week, share what you've noticed about your conversations.

/label ~"status::todo"
/label ~"Crucial Conversations"