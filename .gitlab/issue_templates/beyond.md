<!-- Name this issue "Beyond - Putting it all together" -->
In the weeks following, revisit any skills you noticed you didn't get enough practice on or struggled with.

Continue to log conversations here noting:

- How you prepared for the conversation
- What skills you used in the conversation
- What was the outcome
- Reflect on what other skills could have been useful or done differently

/label ~"status::todo"
/label ~"Crucial Conversations"